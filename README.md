# Kalman Filter for tracking Pedestrians

This is a project derived from the Udacity's Self Driving Car course.
The main objective is to track pedestrians using the Lidar measurements
obtained. These measurements can be found in the `./data/obj_pose-laser*.txt`
